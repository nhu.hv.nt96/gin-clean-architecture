package models

type Migrate struct {
	Id        uint64 `gorm:"column:id;primary_key;auto_increment"`
	Migration string `json:"migration"`
	Version   int    `json:"version"`
}
