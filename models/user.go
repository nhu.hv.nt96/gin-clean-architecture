package models

type Gender uint

const (
	Male Gender = iota
	FeMale
)

func (g Gender) String() string {
	switch g {
	case Male:
		return "Nam"
	}
	return "Nữ"

}

type User struct {
	BaseModel
	FullName  string
	NickName  string
	Birthday  int64
	Phone     string `gorm:"unique"`
	Gender    Gender
	Password  string `json:"-"`
	RoleId    uint64 `json:"-"`
	CreatedBy uint64 `json:"-"`
}

type CreateUserReq struct {
	FullName string `json:"full_name"`
	NickName string `json:"nick_name"`
	Birthday int64  `json:"birthday"`
	Phone    string `json:"phone"`
	Gender   Gender `json:"gender"`
	Password string `json:"password"`
}

type LoginReq struct {
	Phone    string `json:"phone"`
	Password string `json:"password"`
}
