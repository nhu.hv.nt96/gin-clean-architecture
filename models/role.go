package models

import (
	"database/sql/driver"
	"github.com/goccy/go-json"
)

type Role struct {
	BaseModel
	Name       string
	Code       string
	Permission []uint64 `gorm:"type:jsonb"`
}

func (s *Role) Value() (driver.Value, error) {
	valueString, err := json.Marshal(s)
	return string(valueString), err
}

func (s *Role) Scan(value interface{}) error {
	if value == nil {
		return nil
	}
	if err := json.Unmarshal(value.([]byte), &s); err != nil {
		return err
	}
	return nil
}
