package models

type Permission struct {
	BaseModel
	Name string
	Code string
}
