package handler

import (
	"app/models"
	"app/repository"
	"app/usecase"
	"app/utils"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

type IUserHandler interface {
	GetUser(c *gin.Context)
	GetUsers(c *gin.Context)
	CreateUser(c *gin.Context)
	UpdateUser(c *gin.Context)
	DeleteUser(c *gin.Context)
}

func NewUserHandler(postgresRepo *repository.Repositories) IUserHandler {
	return &userHandler{
		useCase: usecase.NewUserUseCase(postgresRepo),
	}
}

type userHandler struct {
	useCase usecase.IUserUseCase
}

func (u *userHandler) GetUser(c *gin.Context) {
	loginUser, err := utils.GetLoggedInUser(c)
	if err != nil {
		Reject(c, http.StatusForbidden, err.Error(), nil)
		return
	}
	user, err2 := u.useCase.GetUser(loginUser.UserId)
	if err != nil {
		Reject(c, http.StatusInternalServerError, err2.Message, nil)
		return
	}
	Resolve(c, http.StatusOK, user)
}

func (u *userHandler) GetUsers(c *gin.Context) {
	users, err := u.useCase.GetUsers()
	if err != nil {
		Reject(c, http.StatusInternalServerError, err.Message, nil)
		return
	}
	Resolve(c, http.StatusOK, users)
}

func (u *userHandler) CreateUser(c *gin.Context) {
	loginUser, err := utils.GetLoggedInUser(c)
	if err != nil {
		Reject(c, http.StatusForbidden, err.Error(), nil)
		return
	}
	var req models.CreateUserReq
	if err := c.ShouldBindJSON(&req); err != nil {
		Reject(c, http.StatusInternalServerError, err.Error(), nil)
		return
	}
	if err2 := u.useCase.CreateUser(loginUser.UserId, req); err2 != nil {
		Reject(c, http.StatusInternalServerError, err2.Message, nil)
		return
	}
	Resolve(c, http.StatusOK, make(map[string]interface{}))
}

func (u *userHandler) UpdateUser(c *gin.Context) {
	id := c.Param("id")
	if id == "" {
		Reject(c, http.StatusBadRequest, "Invalid user_id", nil)
	}
	var req models.CreateUserReq
	if err := c.ShouldBindJSON(&req); err != nil {
		Reject(c, http.StatusInternalServerError, err.Error(), nil)
		return
	}
	userId, err := strconv.ParseUint(id, 0, 64)
	if err != nil {
		Reject(c, http.StatusBadRequest, "Invalid user_id", nil)
		return
	}
	if err2 := u.useCase.UpdateUser(userId, req); err2 != nil {
		Reject(c, http.StatusInternalServerError, err2.Message, nil)
		return
	}
	Resolve(c, http.StatusOK, id)
}

func (u *userHandler) DeleteUser(c *gin.Context) {
	id := c.Param("id")
	if id == "" {
		Reject(c, 400, "Invalid user_id", nil)
	}
	userId, err := strconv.ParseUint(id, 0, 64)
	if err != nil {
		Reject(c, http.StatusBadRequest, "Invalid user_id", nil)
	}
	if err2 := u.useCase.DeleteUser(userId); err2 != nil {
		Reject(c, http.StatusInternalServerError, err2.Message, nil)
	}
	Resolve(c, http.StatusOK, id)
}
