package handler

import (
	"github.com/gin-gonic/gin"
)

//func Resolve(c *gin.Context, httpCode int, data interface{}) {
//	c.JSON(httpCode, entity.Response{
//		Status: entity.ResponseStatus{
//			Code:    httpCode,
//			Message: "success",
//		},
//		Result: entity.ResponseResult{Data: data},
//	})
//}

func Resolve(c *gin.Context, httpCode int, data interface{}) {
	c.JSON(httpCode, gin.H{
		"status": gin.H{
			"code":    httpCode,
			"message": "success",
		},
		"result": gin.H{
			"data": data,
		},
	})
}

func Reject(c *gin.Context, httpCode int, message string, data interface{}) {
	c.JSON(httpCode, gin.H{
		"status": gin.H{
			"code":    httpCode,
			"message": message,
		},
		"result": gin.H{
			"data": data,
		},
	})
}

//c.JSONP(http.StatusInternalServerError, gin.H{
//	"error": err,
//})

//c.JSONP(http.StatusOK, gin.H{
//	"access_token": accessToken,
//})
