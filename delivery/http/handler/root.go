package handler

import "app/repository"

type RootHandler struct {
	IAuthHandler
	IUserHandler
}

func NewRootHandler(postgresRepo *repository.Repositories) *RootHandler {
	return &RootHandler{
		IAuthHandler: NewAuthHandler(postgresRepo),
		IUserHandler: NewUserHandler(postgresRepo),
	}
}
