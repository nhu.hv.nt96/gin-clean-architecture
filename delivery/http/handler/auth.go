package handler

import (
	"app/models"
	"app/repository"
	"app/usecase"
	"github.com/gin-gonic/gin"
	"net/http"
)

type authnHandler struct {
	usecase usecase.AuthUseCase
	//service service.Service
}

type IAuthHandler interface {
	Login(c *gin.Context)
	//Logout(c *gin.Context)
	//GetSignMessage(c *gin.Context)
	//LoginWithMetamask(c *gin.Context)
	//LogUserLocation(c *gin.Context)
}

func NewAuthHandler(postgresRepo *repository.Repositories) IAuthHandler {
	return &authnHandler{
		usecase: usecase.NewAuthUseCase(postgresRepo),
	}
}

func (a *authnHandler) Login(c *gin.Context) {
	var req models.LoginReq
	if err := c.ShouldBindJSON(&req); err != nil {
		Reject(c, http.StatusInternalServerError, err.Error(), nil)
		return
	}
	accessToken, err := a.usecase.Login(req)
	if err != nil {
		Reject(c, http.StatusInternalServerError, "error", nil)
		return
	}
	Resolve(c, http.StatusOK, accessToken)
}
