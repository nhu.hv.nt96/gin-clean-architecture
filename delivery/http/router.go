package http

import (
	"app/middleware"
	"github.com/gin-gonic/gin"

	"app/delivery/http/handler"
)

func InitRoutes(r *gin.Engine, rootHandler *handler.RootHandler, authMiddleware middleware.IAuthMiddleware) *gin.Engine {
	r.GET("/login", rootHandler.Login)

	userAuthGroup := r.Group("/users")
	{
		userAuthGroup.Use(authMiddleware.CheckAuth())
		userAuthGroup.GET("me", rootHandler.GetUser)
	}

	adminAuthGroup := r.Group("/admin")
	{
		adminAuthGroup.Use(authMiddleware.CheckAuth())
		adminAuthGroup.GET("users", rootHandler.GetUsers)
		adminAuthGroup.POST("user", rootHandler.CreateUser)
		adminAuthGroup.PUT("user/:id", rootHandler.UpdateUser)
		adminAuthGroup.DELETE("user/:id", rootHandler.DeleteUser)
	}
	return r
}
