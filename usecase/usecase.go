package usecase

import (
	"app/models"
	"app/repository"
	"app/utils"
	"net/http"
)

type AuthUseCase interface {
	Login(req models.LoginReq) (string, *models.Error)
}

type authUseCase struct {
	postgresRepo *repository.Repositories
}

func NewAuthUseCase(postgresRepo *repository.Repositories) AuthUseCase {
	return &authUseCase{
		postgresRepo: postgresRepo,
	}
}

func (l *authUseCase) Login(req models.LoginReq) (string, *models.Error) {
	check, err := l.postgresRepo.GetUserByPhone(req.Phone)
	if err != nil || check.Password != req.Password {
		return "", models.NewError(2, "Sai teen dang nhap or mat khau")
	}
	user, err := l.postgresRepo.GetUserById(check.Id)
	if err != nil {
		return "", models.NewError(http.StatusInternalServerError, "Server error, please try again")
	}
	accessToken, err := utils.GenerateToken(user)
	if err != nil {
		return "", models.NewError(http.StatusInternalServerError, "Server error, please try again")
	}
	return accessToken, nil
}
