package usecase

import (
	"app/models"
	"app/repository"
	"net/http"
)

type IUserUseCase interface {
	GetUser(userId uint64) (*models.User, *models.Error)
	GetUsers() ([]*models.User, *models.Error)
	CreateUser(userId uint64, req models.CreateUserReq) *models.Error
	UpdateUser(userId uint64, req models.CreateUserReq) *models.Error
	DeleteUser(userId uint64) *models.Error
}

type userUseCase struct {
	postgresRepo *repository.Repositories
}

func NewUserUseCase(postgresRepo *repository.Repositories) IUserUseCase {
	return &userUseCase{
		postgresRepo: postgresRepo,
	}
}

func (u *userUseCase) GetUser(userId uint64) (*models.User, *models.Error) {
	user, err := u.postgresRepo.GetUserById(userId)
	if err != nil {
		return nil, models.NewError(http.StatusInternalServerError, err.Error())
	}
	return user, nil
}

func (u *userUseCase) GetUsers() ([]*models.User, *models.Error) {
	users, err := u.postgresRepo.GetUsers()
	if err != nil {
		return nil, models.NewError(http.StatusInternalServerError, err.Error())
	}
	return users, nil
}

func (u *userUseCase) CreateUser(userId uint64, req models.CreateUserReq) *models.Error {
	newUser := &models.User{
		FullName:  req.FullName,
		NickName:  req.NickName,
		Birthday:  req.Birthday,
		Gender:    req.Gender,
		Phone:     req.Phone,
		CreatedBy: userId,
		Password:  req.Password,
	}
	if err := u.postgresRepo.CreateUser(newUser); err != nil {
		return models.NewError(http.StatusInternalServerError, err.Error())
	}
	return nil
}

func (u *userUseCase) UpdateUser(userId uint64, req models.CreateUserReq) *models.Error {
	user := &models.User{
		FullName: req.FullName,
		NickName: req.NickName,
		Birthday: req.Birthday,
		Phone:    req.Phone,
		Gender:   req.Gender,
	}
	if err := u.postgresRepo.UpdateUserById(userId, user); err != nil {
		return models.NewError(http.StatusInternalServerError, err.Error())
	}
	return nil
}

func (u *userUseCase) DeleteUser(userId uint64) *models.Error {
	if err := u.postgresRepo.DeleteUserId(userId); err != nil {
		return models.NewError(http.StatusInternalServerError, err.Error())
	}
	return nil
}
