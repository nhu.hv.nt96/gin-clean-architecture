package repository

import (
	"app/models"
	"gorm.io/gorm"
)

type IUserRepository interface {
	CreateUser(user *models.User) error
	GetUserById(userId uint64) (*models.User, error)
	GetUserByPhone(phone string) (*models.User, error)
	GetUsers() ([]*models.User, error)
	UpdateUserById(userId uint64, user *models.User) error
	DeleteUserId(userId uint64) error
}

type userRepo struct {
	db *gorm.DB
}

func NewUserRepository(db *gorm.DB) IUserRepository {
	return &userRepo{
		db: db,
	}
}

func (u *userRepo) CreateUser(user *models.User) error {
	return u.db.Create(user).Error
}

func (u *userRepo) GetUserById(userId uint64) (*models.User, error) {
	var result *models.User
	if err := u.db.Take(&result).Where("id = ?", userId).Error; err != nil {
		return nil, err
	}
	return result, nil
}

func (u *userRepo) GetUsers() ([]*models.User, error) {
	var results []*models.User
	if err := u.db.Find(&results).Error; err != nil {
		return nil, err
	}
	return results, nil
}

func (u *userRepo) GetUserByPhone(phone string) (*models.User, error) {
	var result *models.User
	if err := u.db.Take(&result).Where("phone = ?", phone).Error; err != nil {
		return nil, err
	}
	return result, nil
}

func (u *userRepo) UpdateUserById(userId uint64, user *models.User) error {
	if err := u.db.Debug().Where("id = ?", userId).Updates(user).Error; err != nil {
		return err
	}
	return nil
}

func (u *userRepo) DeleteUserId(userId uint64) error {
	return u.db.Where("id = ?", userId).Delete(&models.User{}).Error
}
