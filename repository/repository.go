package repository

import (
	"app/models"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"

	"app/config"
)

//type IUserRepository

type Repositories struct {
	IMigrateRepository
	IUserRepository
	Db *gorm.DB
}

func NewRepositories() (*Repositories, error) {
	db, err := gorm.Open(postgres.Open(config.Env().GetPostgresDSN()), &gorm.Config{
		//Logger: GetLogger(),
	})
	if err != nil {
		return nil, err
	}
	return &Repositories{
		IMigrateRepository: NewMigrateRepository(db),
		IUserRepository:    NewUserRepository(db),
		Db:                 db,
	}, nil
}

func (s *Repositories) AutoMigrate() error {
	return s.Db.AutoMigrate(&models.Migrate{}, &models.User{})
}

func (s *Repositories) AutoMigrateUp(version int) error {
	return s.Execute(version)
}
