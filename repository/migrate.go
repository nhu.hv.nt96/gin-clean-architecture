package repository

import (
	"fmt"
	"reflect"

	"github.com/sirupsen/logrus"
	"gorm.io/gorm"

	"app/models"
)

type migrateRepo struct {
	db *gorm.DB
}

func NewMigrateRepository(db *gorm.DB) IMigrateRepository {
	return &migrateRepo{db}
}

type IMigrateRepository interface {
	Execute(version int) error
}

func (m *migrateRepo) Execute(version int) error {
	var (
		migrate       models.Migrate
		migrateNoFrom int
	)
	if err := m.db.Order("version desc").Where("version <= ?", version).Take(&migrate).Error; err != nil && err == gorm.ErrRecordNotFound {
		migrateNoFrom = 1
	} else {
		migrateNoFrom = migrate.Version + 1
	}

	for i := migrateNoFrom; i <= version; i++ {
		callableName := fmt.Sprintf("Migrate%dUp", i)
		callable := reflect.ValueOf(m).MethodByName(callableName)
		if !callable.IsValid() {
			logrus.Errorf("\nFailed to find Registry method with name \"%s\".", callableName)
		}
		callable.Call([]reflect.Value{})
		migrate := models.Migrate{
			Migration: callableName,
			Version:   i,
		}
		_ = m.db.Create(&migrate).Error
	}
	return nil
}

func (m *migrateRepo) Migrate1Up() {
	m.db.Create(&models.User{
		FullName: "nhuhuynh",
		NickName: "huynhnhu",
		Phone:    "0914885317",
		Password: "12341234",
	})
	logrus.Infoln("Migrate1Up")
}
