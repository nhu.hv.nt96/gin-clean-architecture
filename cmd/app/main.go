package main

import (
	"app/config"
	"app/middleware"
	"app/repository"
	"fmt"
	"github.com/sirupsen/logrus"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"

	"app/delivery/http/handler"

	router "app/delivery/http"
)

func main() {
	startHttpServer()
}

func startHttpServer() {
	initGinHttp()
}

func initGinHttp() {
	gin.SetMode(config.Env().GetGinMode())
	r := gin.Default()
	postgresRepo, err := repository.NewRepositories()
	_ = postgresRepo.AutoMigrate()
	_ = postgresRepo.AutoMigrateUp(1)
	if err != nil {
		logrus.Fatal(err)
	}
	authMiddleware := middleware.NewAuthMiddleware()
	rootHandler := handler.NewRootHandler(postgresRepo)

	router.InitRoutes(r, rootHandler, authMiddleware)
	s := &http.Server{
		Addr:           fmt.Sprintf(":%d", config.Env().GetServerPort()),
		Handler:        r,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
	s.ListenAndServe()
}
