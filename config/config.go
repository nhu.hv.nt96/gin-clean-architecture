package config

import (
	"github.com/joho/godotenv"
	"github.com/sirupsen/logrus"
	"os"

	"github.com/caarlos0/env/v6"
)

var _env AppEnv

func init() {
	_env = &appEnv{}
	_env.loadEnvFromFile()

	logrus.Info("Initialized app environment successfully!")
	print("Initialized app environment successfully!")
}

func Env() AppEnv {
	return _env
}

type envConfig struct {
	ServerPort   int    `env:"SERVER_PORT" envDefault:"8080"`
	GinMode      string `env:"GIN_MODE"`
	AccessSecret string `env:"ACCESS_SECRET"`
	TokenExpire  int    `env:"TOKEN_EXPIRE"`
	PostgresDNS  string `env:"POSTGRES_DSN"`
}

type AppEnv interface {
	loadEnvFromFile()
	GetServerPort() int
	GetGinMode() string
	GetAccessSecret() string
	GetTokenExpire() int
	GetPostgresDSN() string
}

type appEnv struct {
	config envConfig
}

// loadEnvFromFile load and parse environment variables
func (ae *appEnv) loadEnvFromFile() {
	envFile := os.Getenv("ENV_FILE")
	if envFile == "" {
		envFile = "../../.env"
	}

	loadEnvErr := godotenv.Load(envFile)
	if loadEnvErr != nil {
		logrus.Fatal(loadEnvErr)
	}

	parseErr := env.Parse(&ae.config)
	if parseErr != nil {
		logrus.Fatal(parseErr)
	}
}

func (ae *appEnv) GetServerPort() int {
	return ae.config.ServerPort
}

func (ae *appEnv) GetGinMode() string {
	return ae.config.GinMode
}

func (ae *appEnv) GetAccessSecret() string {
	return ae.config.AccessSecret
}

func (ae *appEnv) GetTokenExpire() int {
	return ae.config.TokenExpire
}

func (ae *appEnv) GetPostgresDSN() string {
	return ae.config.PostgresDNS
}
