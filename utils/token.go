package utils

import (
	"app/constant"
	"app/models"
	"context"
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v4"

	"app/config"
)

type Claims struct {
	FullName string `json:"full_name"`
	NickName string `json:"nick_name"`
	UserId   uint64 `json:"user_id"`
	Expire   int64  `json:"expire"`
}

func (c *Claims) Valid() error {
	return nil
}

func GenerateToken(user *models.User) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, &Claims{
		FullName: user.FullName,
		NickName: user.NickName,
		UserId:   user.Id,
		Expire:   time.Now().Add(time.Duration(config.Env().GetTokenExpire()) * time.Hour).Unix(),
	})
	tokenString, err := token.SignedString([]byte(config.Env().GetAccessSecret()))
	return tokenString, err
}

func SignClaim(claim jwt.MapClaims) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claim)
	tokenString, err := token.SignedString([]byte(config.Env().GetAccessSecret()))
	return tokenString, err
}

func VerifyToken(jwtToken string) (jwt.MapClaims, error) {
	token, err := jwt.Parse(jwtToken, func(token *jwt.Token) (interface{}, error) {
		// Don't forget to validate the alg is what you expect:
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		// hmacSampleSecret is a []byte containing your secret, e.g. []byte("my_secret_key")
		return []byte(config.Env().GetAccessSecret()), nil
	})
	if err != nil {
		return nil, err
	}
	claims, ok := token.Claims.(jwt.MapClaims)
	if ok && token.Valid {
		expireTime := claims["expire"].(float64)
		if int64(expireTime) < time.Now().Unix()+10 { // add 10 second to avoid network lag
			return nil, errors.New("token expired")
		}
	} else {
		return nil, errors.New("invalid token")
	}
	return claims, nil
}

func GetSignature(c *gin.Context, jwtToken string) (string, error) {
	token, err := jwt.ParseWithClaims(jwtToken, &Claims{}, func(token *jwt.Token) (interface{}, error) {
		//Make sure that the token method conform to "SigningMethodHMAC"
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return false, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(config.Env().GetAccessSecret()), nil
	})
	if err != nil {
		return "", err
	}
	if claims, ok := token.Claims.(*Claims); ok && token.Valid && claims.Valid() == nil {
		c.Set(constant.LoggedInUser, claims)
		c.Request.WithContext(context.WithValue(c.Request.Context(), constant.LoggedInUser, jwtToken))
	} else {
		return "", claims.Valid()
	}
	return token.Signature, nil
}

// ExtractToken get the token from the request body
func ExtractToken(r *http.Request) string {
	bearToken := r.Header.Get("Authorization")
	strArr := strings.Split(bearToken, " ")
	if len(strArr) == 2 || strArr[0] == "Bearer" {
		return strArr[1]
	}
	return ""
}

func GetLoggedInUser(c *gin.Context) (*Claims, error) {
	user, exist := c.Get(constant.LoggedInUser)
	if exist == true {
		loginUser := user.(*Claims)
		return loginUser, nil
	}
	return nil, errors.New("Authorization")
}
