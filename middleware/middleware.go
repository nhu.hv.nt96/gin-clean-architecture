package middleware

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"app/delivery/http/handler"
	"app/utils"
)

type IAuthMiddleware interface {
	CheckAuth() gin.HandlerFunc
}

func NewAuthMiddleware() IAuthMiddleware {
	return &authMiddleware{}
}

type authMiddleware struct {
}

func (a *authMiddleware) CheckAuth() gin.HandlerFunc {
	return func(c *gin.Context) {
		token := utils.ExtractToken(c.Request)
		if token == "" {
			handler.Reject(c, http.StatusUnauthorized, http.StatusText(http.StatusUnauthorized), nil)
			c.Abort()
			return
		}
		signature, err := utils.GetSignature(c, token)
		if signature == "" || err != nil {
			handler.Reject(c, http.StatusUnauthorized, http.StatusText(http.StatusUnauthorized), nil)
			c.Abort()
			return
		}
		c.Next()
	}
}
